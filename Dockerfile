# Debian Buster Slim
FROM rust:slim-buster

# Run build essential
RUN apt-get update
RUN apt-get install -y cmake g++ make curl git

# install Circom
# ref :- https://docs.circom.io/getting-started/installation/
WORKDIR "/home"
RUN git clone https://github.com/iden3/circom.git 
RUN cd circom && cargo build --release
#RUN echo $(ls -1)
WORKDIR "/home/circom"
RUN cargo install --path circom

# install Nodejs and SNARKjs
RUN curl -fsSL https://deb.nodesource.com/setup_16.x | bash -
RUN apt-get install -y nodejs
RUN npm install -g snarkjs

WORKDIR "/home"



