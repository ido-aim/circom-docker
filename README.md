# circom-docker

Docker for circom/snarkjs using Rust-Debian Image

## Description
I'm using rust image as a docker's base https://hub.docker.com/_/rust. Feel free to improve the Dockerfile

Specification:<br>
- Debian Buster-Slim <br>
- Rust version : 1.57 <br>
- Nodejs version : 16.13.1 <br>
- Circom version : 2.0.3 (JAN,2022)<br>
- Total Image size ~ 2.19GB
## Visuals


## Installation
### 1 Rebuild the image
We could clone the repository <br>
`git clone https://gitlab.com/ido-aim/circom-docker.git`.
Then, try building the image file
`cd circom-docker` <br>
`docker build -t aim-ido/circom:1.0 .`

### 2 Or simply just pull image from my gitlab
`docker pull registry.gitlab.com/ido-aim/circom-docker`

## Usage
just simply run docker container with the built image
`docker run -it aim-ido/circom /bin/bash`
then verify Circom and Snarkjs
`circom -V` <br
`snarkjs --help`




## Improvement
**JAN2022**: I could not create Alpine image since `musl library` in Alpine could not build `Circom` like `Debian` does with `glibc library`. The project would be much better if we could build a smaller image.

## Acknowledgment
Best regards to those who created Circom,Snarkjs, Rust, and Nodejs


.

## License
MIT

